import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_buyer/model/mobile.dart';
import 'package:mobile_buyer/view/mobile_page.dart';

class CardMobile extends StatelessWidget {
  final Mobile mobile;
  final VoidCallback onFavouriteTab;
  final bool showFavouriteIcon;
  CardMobile(this.mobile,
      {@required this.onFavouriteTab, this.showFavouriteIcon = false});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.to(MobilePage(mobile));
      },
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: CachedNetworkImage(
              imageUrl: mobile.thumbImageURL,
              placeholder: (context, url) =>
                  Center(child: CircularProgressIndicator()),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
          Expanded(
            flex: 4,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(
                          "${mobile.brand} ${mobile.name}",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      if (showFavouriteIcon)
                        Container(
                          alignment: Alignment.centerRight,
                          child: InkWell(
                              key: Key(mobile.id.toString()),
                              onTap: this.onFavouriteTab,
                              child: Icon(
                                  mobile.isFavourite
                                      ? Icons.favorite
                                      : Icons.favorite_border,
                                  color: Colors.lightBlue)),
                        )
                    ],
                  ),
                ),
                Container(
                  child: Text(
                    mobile.description,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    softWrap: false,
                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      Text("Price: \$ ${mobile.price}"),
                      Expanded(
                        child: Text(
                          "Rating: \$ ${mobile.rating}",
                          textAlign: TextAlign.end,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
