import 'package:flutter/material.dart';
import 'package:mobile_buyer/model/mobile.dart';
import 'package:mobile_buyer/viewmodel/mobile_list_view_model.dart';
import 'package:mobile_buyer/widgets/card_mobile.dart';
import 'package:provider/provider.dart';

class MobileList extends StatelessWidget {
  final List<Mobile> mobiles;

  MobileList({this.mobiles});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: this.mobiles.length,
      itemBuilder: (context, index) {
        return Card(
          key: Key("list${mobiles[index].id}"),
            child: CardMobile(
          mobiles[index],
          onFavouriteTab: () {
            Provider.of<MobileListViewModel>(context, listen: false)
                .toggleFavourite(index);
          },
          showFavouriteIcon: true,
        ));
      },
    );
  }
}
