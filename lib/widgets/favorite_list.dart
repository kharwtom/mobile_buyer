import 'package:flutter/material.dart';
import 'package:mobile_buyer/model/mobile.dart';
import 'package:mobile_buyer/widgets/card_mobile.dart';

class MobileList extends StatelessWidget {
  final List<Mobile> mobiles;

  MobileList({this.mobiles});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: this.mobiles.length,
      itemBuilder: (context, index) {
        final m = this.mobiles[index];
        return Card(
            key: Key("fav${mobiles[index].id}"),
            child: CardMobile(
              m,
              onFavouriteTab: () {},
            ));
      },
    );
  }
}
