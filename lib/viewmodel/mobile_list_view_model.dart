import 'package:flutter/material.dart';
import 'package:mobile_buyer/model/mobile.dart';
import 'package:mobile_buyer/services/webservice.dart';

class MobileListViewModel extends ChangeNotifier {
  List<Mobile> mobiles = [];
  Future<void> fetchMobile() async {
    final results = await Webservice().fetchMobile();
    mobiles = results;
    notifyListeners();
  }

  List<Mobile> get favouriteMobiles {
    return mobiles.where((mobile) => mobile.isFavourite).toList();
  }

  void sortByPriceLowToHigh() {
    mobiles.sort((a, b) => a.price.compareTo(b.price));
    notifyListeners();
  }

  void sortByPriceHighToLow() {
    mobiles.sort((a, b) => b.price.compareTo(a.price));
    notifyListeners();
  }

  void sortByRating() {
    mobiles.sort((a, b) => b.rating.compareTo(a.rating));
    notifyListeners();
  }

  void toggleFavourite(int mobileIndex) {
    mobiles[mobileIndex].isFavourite = !mobiles[mobileIndex].isFavourite;
    notifyListeners();
  }
}
