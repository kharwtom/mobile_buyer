import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mobile_buyer/model/mobile.dart';

class Webservice {
  final String domain = "https://scb-test-mobile.herokuapp.com/api";
  Future<List<Mobile>> fetchMobile() async {
    final url = Uri.parse(domain + '/mobiles');
    final response = await http.get(url);
    if (response.statusCode == 200) {
      final jsonResponse = jsonDecode(response.body);
      return List<Mobile>.from(
          jsonResponse.map((mobile) => Mobile.fromJson(mobile)));
    } else {
      throw Exception("Unable to perform request!");
    }
  }
}
