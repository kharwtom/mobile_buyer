import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mobile_buyer/model/mobile.dart';

class MobilePage extends StatelessWidget {
  final Mobile mobile;
  const MobilePage(this.mobile);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("MOBILE PHONE"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            CachedNetworkImage(
              imageUrl: mobile.thumbImageURL,
              placeholder: (context, url) =>
                  Center(child: CircularProgressIndicator()),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            Text(
              "${mobile.brand} ${mobile.name}",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 20,
            ),
            Text("${mobile.description}"),
          ],
        ),
      ),
    );
  }
}
