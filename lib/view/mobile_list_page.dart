import 'package:flutter/material.dart';
import 'package:mobile_buyer/viewmodel/mobile_list_view_model.dart';
import 'package:mobile_buyer/widgets/mobile_list.dart';

import 'package:provider/provider.dart';

class MobileListPage extends StatelessWidget {
  const MobileListPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final vm = Provider.of<MobileListViewModel>(context);

    return Scaffold(
        backgroundColor: Colors.grey[300],
        body: Container(
            padding: EdgeInsets.all(10),
            child: Column(children: <Widget>[
              Expanded(child: MobileList(mobiles: vm.mobiles))
            ])));
  }
}
