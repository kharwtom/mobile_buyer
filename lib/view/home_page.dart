import 'package:flutter/material.dart';
import 'package:mobile_buyer/view/favorite_list_page.dart';
import 'package:mobile_buyer/view/mobile_page.dart';
import 'package:mobile_buyer/viewmodel/mobile_list_view_model.dart';
import 'package:provider/provider.dart';
import 'package:get/get.dart';

import 'mobile_list_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    Provider.of<MobileListViewModel>(context, listen: false).fetchMobile();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(
                key: Key("all"),
                text: 'mobile list'.toUpperCase(),
              ),
              Tab(
                key: Key("favourite"),
                text: 'favorite list'.toUpperCase(),
              ),
            ],
          ),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('MOBILE PHONE'),
              InkWell(
                key: Key("sort"),
                onTap: () {
                  Get.defaultDialog(
                    title: "Sort",
                    content: Wrap(
                      alignment: WrapAlignment.center,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.center,
                              height: 50,
                              child: InkWell(
                                key: Key("sort_asc"),
                                onTap: () {
                                  Provider.of<MobileListViewModel>(context,
                                          listen: false)
                                      .sortByPriceLowToHigh();
                                  Get.back();
                                },
                                child: Text(
                                  "Price low to high",
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Container(
                              alignment: Alignment.center,
                              height: 50,
                              child: InkWell(
                                key: Key("sort_desc"),
                                onTap: () {
                                  Provider.of<MobileListViewModel>(context,
                                          listen: false)
                                      .sortByPriceHighToLow();
                                  Get.back();
                                },
                                child: Text(
                                  "Price high to low",
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Container(
                              alignment: Alignment.center,
                              height: 50,
                              child: InkWell(
                                key: Key("sort_rating"),
                                onTap: () {
                                  Provider.of<MobileListViewModel>(context,
                                          listen: false)
                                      .sortByRating();
                                  Get.back();
                                },
                                child: Text(
                                  "Rating",
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                },
                child: Text("Sort"),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [MobileListPage(), FavoriteListPage()],
        ),
      ),
    );
  }
}
