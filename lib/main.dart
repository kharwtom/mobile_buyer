import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_buyer/view/home_page.dart';
import 'package:mobile_buyer/viewmodel/mobile_list_view_model.dart';

import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        home: ChangeNotifierProvider(
          create: (context) => MobileListViewModel(),
          child: HomePage(),
        ));
  }
}
