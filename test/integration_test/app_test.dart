import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mobile_buyer/main.dart' as app;
import 'package:mobile_buyer/viewmodel/mobile_list_view_model.dart';
import 'package:integration_test/integration_test.dart';

void main() {
  group("Mobile phone buyer app test ", () {
    IntegrationTestWidgetsFlutterBinding.ensureInitialized();
    testWidgets("full app test", (tester) async {
      app.main();
      await tester.pumpAndSettle();
      MobileListViewModel viewModel = MobileListViewModel();
      await viewModel.fetchMobile();
      await tester.pumpAndSettle();
      final cardMobile = find.byKey(Key("list${viewModel.mobiles[0].id}"));
      final favourite = find.byKey(Key(viewModel.mobiles[0].id.toString()));
      final sortInkwell = find.byKey(Key("sort"));
      final sortAscInkwell = find.byKey(Key("sort_asc"));
      final sortDescInkwell = find.byKey(Key("sort_desc"));
      final sortRating = find.byKey(Key("sort_rating"));
      final favouriteTab = find.byKey(Key("favourite"));
      final allTab = find.byKey(Key("all"));
      await tester.tap(favourite);
      await tester.pumpAndSettle();
      await tester.tap(favouriteTab);
      await tester.pumpAndSettle();
      await tester.tap(allTab);
      await tester.pumpAndSettle();
      await tester.tap(sortInkwell);
      await tester.pumpAndSettle();
      await tester.tap(sortAscInkwell);
      await tester.pumpAndSettle();
      await tester.tap(sortInkwell);
      await tester.pumpAndSettle();
      await tester.tap(sortDescInkwell);
      await tester.pumpAndSettle();
      await tester.tap(sortInkwell);
      await tester.pumpAndSettle();
      await tester.tap(sortRating);
      await tester.pumpAndSettle();
      await tester.tap(sortInkwell);
      await tester.pumpAndSettle();
      await tester.tap(cardMobile);
      await tester.pumpAndSettle();
    });
  });
}
