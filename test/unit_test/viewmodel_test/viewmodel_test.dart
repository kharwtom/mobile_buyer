import 'package:mobile_buyer/model/mobile.dart';
import 'package:test/test.dart';
import 'package:mobile_buyer/viewmodel/mobile_list_view_model.dart';

void main() {
  group('Card viewmodel', () {
    group('favouriteMobiles', () {
      test('should get empty list', () async {
        MobileListViewModel viewModel = MobileListViewModel();
        viewModel.mobiles = <Mobile>[
          Mobile(
              rating: 10,
              brand: "test",
              description: "for test card viewmodel",
              id: 1,
              name: "test",
              price: 20.0,
              thumbImageURL: "www.test.com"),
          Mobile(
              rating: 30,
              brand: "test2",
              description: "for test card viewmodel2",
              id: 2,
              name: "test2",
              price: 30.0,
              thumbImageURL: "www.test2.com")
        ];

        expect(viewModel.favouriteMobiles.length, 0);
      });

      test('should get favourite list', () async {
        MobileListViewModel viewModel = MobileListViewModel();
        viewModel.mobiles = <Mobile>[
          Mobile(
              rating: 10,
              brand: "test",
              description: "for test card viewmodel",
              id: 1,
              name: "test",
              price: 20.0,
              thumbImageURL: "www.test.com"),
          Mobile(
              rating: 30,
              brand: "test2",
              description: "for test card viewmodel2",
              id: 2,
              name: "test2",
              price: 30.0,
              thumbImageURL: "www.test2.com")
        ];
        viewModel.mobiles[0].isFavourite = true;
        expect(viewModel.favouriteMobiles.length, 1);
        expect(viewModel.favouriteMobiles[0].rating, 10);
        expect(viewModel.favouriteMobiles[0].description,
            "for test card viewmodel");
        expect(viewModel.favouriteMobiles[0].id, 1);
        expect(viewModel.favouriteMobiles[0].name, "test");
        expect(viewModel.favouriteMobiles[0].price, 20.0);
        expect(viewModel.favouriteMobiles[0].thumbImageURL, "www.test.com");
      });
    });

    group('fetch model', () {
      test('should get mobiles list', () async {
        MobileListViewModel viewModel = MobileListViewModel();
        await viewModel.fetchMobile();

        expect(viewModel.mobiles.length > 0, true);
      });
    });

    group('toggleFavourite', () {
      test('should able to toggle twice correctly', () async {
        MobileListViewModel viewModel = MobileListViewModel();
        viewModel.mobiles = <Mobile>[
          Mobile(
              rating: 10,
              brand: "test",
              description: "for test card viewmodel",
              id: 1,
              name: "test",
              price: 20.0,
              thumbImageURL: "www.test.com"),
          Mobile(
              rating: 30,
              brand: "test2",
              description: "for test card viewmodel2",
              id: 2,
              name: "test2",
              price: 30.0,
              thumbImageURL: "www.test2.com")
        ];
        viewModel.toggleFavourite(0);
        expect(viewModel.mobiles[0].isFavourite, true);
        viewModel.toggleFavourite(0);
        expect(viewModel.mobiles[0].isFavourite, false);
      });
    });

    group('sortMobiles', () {
      test('Should get sort mobiles by price low to high', () async {
        MobileListViewModel viewModel = MobileListViewModel();
        viewModel.mobiles = <Mobile>[
          Mobile(
              rating: 10,
              brand: "test",
              description: "for test card viewmodel",
              id: 1,
              name: "test",
              price: 20.0,
              thumbImageURL: "www.test.com"),
          Mobile(
              rating: 30,
              brand: "test2",
              description: "for test card viewmodel2",
              id: 2,
              name: "test2",
              price: 30.0,
              thumbImageURL: "www.test2.com")
        ];
        viewModel.sortByPriceLowToHigh();
        expect(viewModel.mobiles[0].price < viewModel.mobiles[1].price, true);
      });

      test('Should get sort mobiles by price high to low', () async {
        MobileListViewModel viewModel = MobileListViewModel();
        viewModel.mobiles = <Mobile>[
          Mobile(
              rating: 10,
              brand: "test",
              description: "for test card viewmodel",
              id: 1,
              name: "test",
              price: 20.0,
              thumbImageURL: "www.test.com"),
          Mobile(
              rating: 30,
              brand: "test2",
              description: "for test card viewmodel2",
              id: 2,
              name: "test2",
              price: 30.0,
              thumbImageURL: "www.test2.com")
        ];
        viewModel.sortByPriceHighToLow();
        expect(viewModel.mobiles[0].price > viewModel.mobiles[1].price, true);
      });

      test('Should get sort mobiles by rating', () async {
        MobileListViewModel viewModel = MobileListViewModel();
        viewModel.mobiles = <Mobile>[
          Mobile(
              rating: 10,
              brand: "test",
              description: "for test card viewmodel",
              id: 1,
              name: "test",
              price: 20.0,
              thumbImageURL: "www.test.com"),
          Mobile(
              rating: 30,
              brand: "test2",
              description: "for test card viewmodel2",
              id: 2,
              name: "test2",
              price: 30.0,
              thumbImageURL: "www.test2.com")
        ];
        viewModel.sortByRating();
        expect(viewModel.mobiles[0].rating > viewModel.mobiles[1].rating, true);
      });
    });
  });
}
