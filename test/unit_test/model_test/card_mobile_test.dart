import 'package:mobile_buyer/model/mobile.dart';
import 'package:test/test.dart';

void main() {
  group('Mobile model', () {
    test('should parse json to mobile model correctly', () async {
      dynamic json = {
        "thumbImageURL":
            "https://cdn.mos.cms.futurecdn.net/grwJkAGWQp4EPpWA3ys3YC-650-80.jpg",
        "price": 179.99,
        "rating": 4.9,
        "brand": "Samsung",
        "id": 1,
        "name": "Moto E4 Plus",
        "description":
            "First place in our list goes to the excellent Moto E4 Plus. It's a cheap phone that features phenomenal battery life, a fingerprint scanner and a premium feel design, plus it's a lot cheaper than the Moto G5 below. It is a little limited with its power, but it makes up for it by being able to last for a whole two days from a single charge. If price and battery are the most important features for you, the Moto E4 Plus will suit you perfectly."
      };

      Mobile mobile = Mobile.fromJson(json);

      expect(mobile.thumbImageURL, json['thumbImageURL']);
      expect(mobile.price, json['price']);
      expect(mobile.rating, json['rating']);
      expect(mobile.brand, json['brand']);
      expect(mobile.id, json['id']);
      expect(mobile.name, json['name']);
      expect(mobile.description, json['description']);
      expect(mobile.isFavourite, false);
    });

    test('should parse empty json to mobile model ', () async {
      dynamic json = {};

      Mobile mobile = Mobile.fromJson(json);

      expect(mobile.thumbImageURL, "");
      expect(mobile.price, null);
      expect(mobile.rating, null);
      expect(mobile.brand, "");
      expect(mobile.id, null);
      expect(mobile.name, "");
      expect(mobile.description, "");
      expect(mobile.isFavourite, false);
    });
  });
}
